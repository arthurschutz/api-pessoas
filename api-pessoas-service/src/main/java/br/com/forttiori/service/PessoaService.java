package br.com.forttiori.service;

import br.com.forttiori.entity.Pessoa;
import br.com.forttiori.service.dto.ServiceRequestDTO;
import br.com.forttiori.service.dto.ServiceResponseDTO;

import java.util.List;

public interface PessoaService {

    ServiceResponseDTO save(ServiceRequestDTO serviceRequestDTO);
    Pessoa save (Pessoa pessoa);
    List<ServiceResponseDTO> findAll();
    Pessoa findByIdEntity(String id);
    ServiceResponseDTO findByIdResponse(String id);
    ServiceResponseDTO update(String id, ServiceRequestDTO serviceRequestDTO);
    void deleteById(String id);
    List<Pessoa> deleteMany(List<String> ids);
}
