package br.com.forttiori.controller.exceptionhandler;

import lombok.Builder;
import lombok.Data;


@Data
@Builder
public class StandardError {

    private final int status;
    private final String message;
}
