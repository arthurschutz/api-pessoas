package br.com.forttiori.controller.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ControllerResponseDTO {

    private String id;
    private String name;
    private String email;
}
