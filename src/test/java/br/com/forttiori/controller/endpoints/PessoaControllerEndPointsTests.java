package br.com.forttiori.controller.endpoints;

import br.com.forttiori.controller.PessoaController;
import br.com.forttiori.entity.Pessoa;
import br.com.forttiori.exceptions.PessoaNaoEncontradaException;
import br.com.forttiori.service.PessoaServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

//@WebMvcTest(PessoaController.class)
//class PessoaControllerEndPointsTests {
//
//    @Autowired
//    private MockMvc mockMvc;
//
//    @MockBean
//    PessoaServiceImpl pessoaService;
//
//    @Test
//    void shouldReturnPessoaSavedAndIsCreated() throws Exception {
//        var json = new ObjectMapper().writeValueAsString(pessoaRequestDTO);
//
//        when(this.pessoaService.save(pessoaRequestDTO)).thenReturn(pessoaResponseDTO);
//
//        this.mockMvc.perform(post("http://localhost:8081/pessoas")
//                .contentType(MediaType.APPLICATION_JSON)
//                .content(json))
//                .andExpect(status().isCreated())
//                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
//                .andExpect(jsonPath("id", is("1")))
//                .andExpect(jsonPath("name", is("Arthur")))
//                .andExpect(jsonPath("email", is("arthur@gmail.com")))
//                .andDo(print());
//
//    }
//
//    @Test
//    void shouldReturnAListAndIsOK() throws Exception {
//
//        when(this.pessoaService.findAll()).thenReturn(pessoaResponseDTOList);
//
//        this.mockMvc.perform(get("http://localhost:8081/pessoas/todos"))
//                .andExpect(status().isOk())
//                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
//                .andExpect(jsonPath("$[0].id", is("1")))
//                .andExpect(jsonPath("$[0].name", is("Arthur")))
//                .andExpect(jsonPath("$[0].email", is("arthur@gmail.com")))
//                .andDo(print());
//
//    }
//
//    @Test
//    void shouldReturnPessoaByIdAndIsOK() throws Exception {
//
//        when(this.pessoaService.findByIdResponse("id")).thenReturn(pessoaResponseDTO);
//
//        this.mockMvc.perform(get("http://localhost:8081/pessoas/id"))
//                .andExpect(status().isOk())
//                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
//                .andExpect(jsonPath("id", is("1")))
//                .andExpect(jsonPath("name", is("Arthur")))
//                .andExpect(jsonPath("email", is("arthur@gmail.com")))
//                .andDo(print());
//    }
//
//    @Test
//    void whenUpdateShouldReturnTheUserSavedAndStatusOK() throws Exception {
//        var json = new ObjectMapper().writeValueAsString(this.pessoaRequestDTO);
//
//        when(this.pessoaService.update("1", this.pessoaRequestDTO)).thenReturn(this.pessoaResponseDTO);
//        this.mockMvc.perform(put("http://localhost:8081/pessoas/1")
//                .contentType(MediaType.APPLICATION_JSON)
//                .content(json))
//                .andExpect(status().isOk())
//                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
//                .andExpect(jsonPath("id", is("1")))
//                .andExpect(jsonPath("name", is("Arthur")))
//                .andExpect(jsonPath("email", is("arthur@gmail.com")))
//                .andDo(print());
//    }
//    @Test
//    void deveRetornarPessoaNotFoundNOT_FOUND() throws Exception {
//
//        given(pessoaService.findByIdResponse("id")).willThrow(new PessoaNaoEncontradaException("Pessoa não encontrada"));
//
//        mockMvc.perform(get("http://localhost:8081/pessoas/id")).andExpect(status().isNotFound());
//
//    }
//
//    @Test
//    void deveDeletarUsuarioPorIdAndNoContent() throws Exception {
//
//        this.mockMvc.perform(delete("http://localhost:8081/pessoas/id"))
//                .andExpect(status().isNoContent());
//    }
//
//    @Test
//    void deveDeletarUsuariosPorListaAndIsOk() throws Exception {
//        List<String> ids = Arrays.asList("1","2","3");
//
//        when(this.pessoaService.deleteMany(ids)).thenReturn(listPessoa);
//
//        this.mockMvc.perform(delete("http://localhost:8081/pessoas?ids=1,2,3"))
//                .andExpect(status().isNoContent())
//                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
//                .andExpect(jsonPath("$[0].id", is("1")))
//                .andExpect(jsonPath("$[0].name", is("Arthur")))
//                .andExpect(jsonPath("$[0].email", is("arthur@gmail.com")))
//                .andDo(print());
//    }
//
//
//    PessoaRequestDTO pessoaRequestDTO = PessoaRequestDTO.builder()
//            .name("Arthur")
//            .email("arthur@gmail.com")
//            .senha("1234")
//            .build();
//
//    PessoaResponseDTO pessoaResponseDTO = PessoaResponseDTO.builder()
//            .id("1")
//            .email("arthur@gmail.com")
//            .name("Arthur")
//            .build();
//
//
//    List<Pessoa> listPessoa = Arrays.asList(
//            Pessoa.builder().id("1").name("Arthur").email("arthur@gmail.com").senha("123").build(),
//            Pessoa.builder().id("2").name("Carlos").email("carlos@gmail.com").senha("456").build(),
//            Pessoa.builder().id("3").name("Magda").email("magda@gmail.com").senha("789").build());
//
//    List<PessoaResponseDTO> pessoaResponseDTOList =  Arrays.asList(
//            PessoaResponseDTO.builder().id("1").email("arthur@gmail.com").name("Arthur").build(),
//            PessoaResponseDTO.builder().id("2").email("carlos@gmail.com").name("Carlos").build(),
//            PessoaResponseDTO.builder().id("3").email("magda@gmail.com").name("Magda").build());
//
//}
