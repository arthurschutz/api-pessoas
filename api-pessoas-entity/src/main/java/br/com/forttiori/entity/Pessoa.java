package br.com.forttiori.entity;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Builder
@Document(collection = "pessoas")
public class Pessoa {

    @Id
    private String id;
    private String name;
    private String email;
    private String senha;

}

