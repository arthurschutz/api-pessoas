package br.com.forttiori.service;

//@ExtendWith(MockitoExtension.class)
//class PessoaServiceImplTest {
//
//    @Mock
//    PessoaRepository pessoaRepository;
//
//    @InjectMocks
//    PessoaServiceImpl pessoaService;
//
//    @Test
//    void whenSaveShouldReturnResponse() {
//
//        when(pessoaRepository.save(pessoaIdNulo)).thenReturn(pessoa);
//
//        var atual = this.pessoaService.save(pessoaRequestDTO);
//        var stubExpected = PessoaResponseDTO.mapEntidadeToResponse(pessoa);
//
//        assertEquals(stubExpected, atual);
//
//    }
//
//    @Test
//    void whenSaveShouldReturnPessoa() {
//
//        var pessoaRetorno = pessoa;
//
//        when(pessoaRepository.save(pessoaIdNulo)).thenReturn(pessoa);
//        var pessoaSalva = this.pessoaService.save(pessoaIdNulo);
//
//        assertEquals(pessoaRetorno, pessoaSalva);
//    }
//
//    @Test
//    void whenFindAllShouldReturnAList() {
//
//        listaPessoas.add(pessoaIdNulo);
//
//        when(pessoaRepository.findAll()).thenReturn(listaPessoas);
//
//        var atual = this.pessoaService.findAll();
//        var stubExpected = PessoaResponseDTO.mapListPessoaToListResponse(listaPessoas);
//
//        assertEquals(stubExpected, atual);
//    }
//
//    @Test
//    void whenFindByIdEntityShouldReturnEntity() {
//
//        when(pessoaRepository.findById("1")).thenReturn(Optional.ofNullable(pessoa));
//
//        var atual = this.pessoaService.findByIdEntity("1");
//        var stubExpected = pessoa;
//
//        assertEquals(stubExpected, atual);
//    }
//
//    @Test
//    void whenFindByIdResponseShouldReturnResponse() {
//
//        when(pessoaRepository.findById("1")).thenReturn(Optional.ofNullable(pessoa));
//
//        var atual = this.pessoaService.findByIdResponse("1");
//        var stubExpected = PessoaResponseDTO.mapEntidadeToResponse(pessoa);
//
//        assertEquals(stubExpected, atual);
//
//    }
//
//    @Test
//    void deveLancarExceptionNotFoundEntidade() {
//
//        String mensagemErro = "Pessoa não encontrada";
//
//        PessoaNaoEncontradaException pessoaNaoEncontradaException = assertThrows( PessoaNaoEncontradaException.class,
//                () -> pessoaService.findByIdEntity("abc"));
//
//        assertEquals(mensagemErro, pessoaNaoEncontradaException.getMessage());
//
//    }
//
//    @Test
//    void deveLancarExceptionNotFoundResponse() {
//
//        String mensagemErro = "Pessoa não encontrada";
//
//        PessoaNaoEncontradaException pessoaNaoEncontradaException = assertThrows( PessoaNaoEncontradaException.class,
//                () -> pessoaService.findByIdResponse("abc"));
//
//        assertEquals(mensagemErro, pessoaNaoEncontradaException.getMessage());
//    }
//
//    @Test
//    void whenUpdateShouldReturnPessoaUpdated() {
//
//        when(pessoaRepository.findById("1")).thenReturn(Optional.ofNullable(pessoa));
//        when(pessoaRepository.save(pessoa)).thenReturn(pessoa);
//
//        var atual = this.pessoaService.update("1", pessoaRequestDTO);
//        var stubExpected = PessoaResponseDTO.builder()
//                .id(pessoa.getId())
//                .email(pessoa.getEmail())
//                .name(pessoa.getName())
//                .build();
//
//        assertEquals(stubExpected, atual);
//
//    }
//
//    @Test
//    void whenDeleteByIdShouldReturnNoContent() {
//
//        when(pessoaRepository.existsById(this.pessoa.getId())).thenReturn(true);
//        pessoaService.deleteById(this.pessoa.getId());
//        verify(pessoaRepository, times(1)).deleteById(this.pessoa.getId());
//    }
//
//    @Test
//    void whenDeleteManyShouldReturnNoContent() {
//
//        Iterable<Pessoa> pessoas = listPessoa;
//        Iterable<String> ids = Arrays.asList("1", "2", "3");
//
//        when( pessoaRepository.findAllById( ids ) ).thenReturn( pessoas );
//
//
//        var atual = this.pessoaService.deleteMany((List<String>) ids);
//        var stubExpected = pessoas;
//
//        assertEquals(stubExpected, atual);
//        verify(pessoaRepository, times(1)).deleteAll(pessoas);
//    }
//
//    Pessoa pessoa = Pessoa.builder()
//            .id("1")
//            .name("Arthur")
//            .email("arthur@gmail.com")
//            .senha("1234")
//            .build();
//
//    Pessoa pessoaIdNulo = Pessoa.builder()
//            .id(null)
//            .name("Arthur")
//            .email("arthur@gmail.com")
//            .senha("1234")
//            .build();
//
//    PessoaRequestDTO pessoaRequestDTO = PessoaRequestDTO.builder()
//            .name("Arthur")
//            .email("arthur@gmail.com")
//            .senha("1234")
//            .build();
//
//    List<Pessoa> listaPessoas = new ArrayList<>();
//
//
//    public List<Pessoa> listPessoa = Arrays.asList(
//            Pessoa.builder().id("1").name("Arthur").email("arthur@gmail.com").senha("123").build(),
//            Pessoa.builder().id("2").name("Carlos").email("carlos@gmail.com").senha("456").build(),
//            Pessoa.builder().id("3").name("Magda").email("magda@gmail.com").senha("789").build());
//
//}
