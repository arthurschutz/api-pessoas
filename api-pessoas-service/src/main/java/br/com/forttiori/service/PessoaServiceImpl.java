package br.com.forttiori.service;

import br.com.forttiori.entity.Pessoa;
import br.com.forttiori.exceptions.PessoaNaoEncontradaException;
import br.com.forttiori.repository.PessoaRepository;
import br.com.forttiori.service.dto.MapperService;
import br.com.forttiori.service.dto.ServiceRequestDTO;
import br.com.forttiori.service.dto.ServiceResponseDTO;
import lombok.Builder;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

import static br.com.forttiori.service.dto.MapperService.*;

@Service
@RequiredArgsConstructor
public class PessoaServiceImpl implements PessoaService {

    private final PessoaRepository pessoaRepository;

    private final String MESSAGE_NOT_FOUND_PESSOA = "Pessoa não encontrada";

    public ServiceResponseDTO save(ServiceRequestDTO serviceRequestDTO) {
        var pessoa = this.pessoaRepository.save(mapPessoaToEntity(serviceRequestDTO));
        return MapperService.mapToPessoaServiceResponse(pessoa);
    }

    public Pessoa save (Pessoa pessoa) {
        return this.pessoaRepository.save(pessoa);
    }

    public List<ServiceResponseDTO> findAll() {
        return mapListPessoaToListResponse(
                this.pessoaRepository.findAll());

    }

    public Pessoa findByIdEntity(String id) {
        Optional<Pessoa> user = this.pessoaRepository.findById(id);
        return user.orElseThrow(() -> new PessoaNaoEncontradaException(MESSAGE_NOT_FOUND_PESSOA));
    }

    public ServiceResponseDTO findByIdResponse(String id) {
        return mapToPessoaServiceResponse(this.findByIdEntity(id));
    }

    public ServiceResponseDTO update(String id, ServiceRequestDTO serviceRequestDTO) {
        Pessoa pessoa = this.findByIdEntity(id);

        pessoa.setName(serviceRequestDTO.getName());
        pessoa.setEmail(serviceRequestDTO.getEmail());
        pessoa.setSenha(serviceRequestDTO.getSenha());

        var pessoaPersistida = this.save(pessoa);
        return mapToPessoaServiceResponse(pessoaPersistida);
    }

    public void deleteById(String id) {
        var exists = pessoaRepository.existsById(id);
        if (!exists) {
            throw new PessoaNaoEncontradaException("Pessoa não encontrada!");
        }
        pessoaRepository.deleteById(id);
    }

    public List<Pessoa> deleteMany(List<String> ids) {
        Optional<Iterable<Pessoa>> pessoas = Optional.of(this.pessoaRepository.findAllById(ids));
        pessoas.ifPresent(this.pessoaRepository::deleteAll);
        return (List<Pessoa>) pessoas.orElseThrow(new PessoaNaoEncontradaException("Pessoa não encontrada!"));
    }
}

