package br.com.forttiori.controller;

//@ExtendWith(MockitoExtension.class)
//class PessoaControllerTest {
//
//    @Mock
//    PessoaServiceImpl pessoaService;
//
//    @InjectMocks
//    PessoaController pessoaController;
//
//    @Test
//    void whenSaveShouldReturnResponse() {
//
//        when(pessoaService.save(pessoaRequestDTO)).thenReturn(pessoaResponseDTO);
//
//        var atual = this.pessoaController.save(pessoaRequestDTO);
//        var stubExpected = pessoaResponseDTO;
//
//        assertEquals(stubExpected, atual);
//
//    }
//
//    @Test
//    void whenFindAllShouldReturnAList() {
//
//        var response = mapListPessoaToListResponse( listPessoa );
//
//        when(pessoaService.findAll()).thenReturn(response);
//
//        var atual = this.pessoaController.findAll();
//
//        assertEquals(response, atual);
//
//    }
//
//    @Test
//    void whenFindByIdShouldReturnPessoaById() {
//
//        when(pessoaService.findByIdResponse("1")).thenReturn(pessoaResponseDTO);
//
//        var atual = this.pessoaController.findById("1");
//        var stubExpect = pessoaResponseDTO;
//
//        assertEquals(stubExpect, atual);
//    }
//
//    @Test
//    void whenUpdateShouldReturnPessoaUpdated() {
//
//        when(pessoaService.update("1", pessoaRequestDTO)).thenReturn(pessoaResponseDTO);
//
//        var atual = this.pessoaController.update("1", pessoaRequestDTO);
//        var stubExpect = pessoaResponseDTO;
//
//        assertEquals(stubExpect, atual);
//    }
//
//    @Test
//    void whenDeleteByIdShouldReturnNoContent() {
//
//        pessoaController.delete("1");
//        verify(pessoaService, times(1)).deleteById("1");
//
//    }
//
//    @Test
//    void whenDeleteManyShouldReturnNoContent() {
//
//        List<Pessoa> pessoas = listPessoa;
//        List<String> ids = Arrays.asList("1", "2", "3");
//
//        when(pessoaService.deleteMany(ids)).thenReturn(pessoas);
//
//        var atual = this.pessoaController.deleteMany(ids);
//        var stubExpect = pessoas;
//
//        assertEquals(stubExpect, atual);
//    }
//
//    PessoaRequestDTO pessoaRequestDTO = PessoaRequestDTO.builder()
//            .name("Arthur")
//            .email("arthur@gmail.com")
//            .senha("1234")
//            .build();
//
//    PessoaResponseDTO pessoaResponseDTO = PessoaResponseDTO.builder()
//            .id("1")
//            .email("arthur@gmail.com")
//            .name("Arthur")
//            .build();
//
//
//    public List<Pessoa> listPessoa = Arrays.asList(
//            Pessoa.builder().name("Arthur").email("arthur@gmail.com").senha("123").build(),
//            Pessoa.builder().name("Carlos").email("carlos@gmail.com").senha("456").build(),
//            Pessoa.builder().name("Magda").email("magda@gmail.com").senha("789").build());
//}