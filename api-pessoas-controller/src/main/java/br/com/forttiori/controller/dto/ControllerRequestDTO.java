package br.com.forttiori.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ControllerRequestDTO {

    private final String NOT_NULL = "Não pode ser nulo";
    private final String NOT_EMPTY = "Não pode ser vazio";

    @NotNull(message = NOT_NULL)
    @NotEmpty(message = NOT_EMPTY)
    private String name;
    @NotNull(message = NOT_NULL)
    @NotEmpty(message = NOT_EMPTY)
    private String email;
    @NotNull(message = NOT_NULL)
    @NotEmpty(message = NOT_EMPTY)
    private String senha;
}
