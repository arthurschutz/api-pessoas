package br.com.forttiori;

import static org.junit.jupiter.api.Assertions.assertEquals;

//@ExtendWith(SpringExtension.class)
//@ContextConfiguration(classes = {PessoaController.class, PessoaServiceImpl.class})
//class IntegrationTest {
//
//    @MockBean
//    PessoaRepository pessoaRepository;
//
//    @Autowired
//    PessoaServiceImpl pessoaService;
//
//    @Test
//    void whenSaveShouldReturnResponse() {
//
//        when(pessoaRepository.save(mapPessoaToEntity(pessoaRequestDTO))).thenReturn(pessoa);
//
//        var atual = pessoaService.save(pessoaRequestDTO);
//        var stubExpect = pessoaResponseDTO;
//
//        assertEquals(stubExpect, atual);
//    }
//
//    @Test
//    void whenFindAllShouldReturnAList() {
//
//        List<Pessoa> listaPessoa = new ArrayList<>();
//        listaPessoa.add(pessoa);
//
//        var response = mapListPessoaToListResponse( listaPessoa );
//
//        when(pessoaRepository.findAll()).thenReturn(listaPessoa);
//
//        var atual = this.pessoaService.findAll();
//
//        assertEquals(response, atual);
//
//    }
//
//    @Test
//    void whenFindByIdShouldReturnPessoaById() {
//
//        when(pessoaRepository.findById("1")).thenReturn(Optional.ofNullable(pessoa));
//
//        var atual = this.pessoaService.findByIdResponse("1");
//        var stubExpect = pessoaResponseDTO;
//
//        assertEquals(stubExpect, atual);
//    }
//
//    @Test
//    void whenUpdateShouldReturnPessoaUpdated() {
//
//        when(pessoaRepository.findById("1")).thenReturn(Optional.ofNullable(pessoa));
//        when(pessoaRepository.save(pessoa)).thenReturn(pessoa);
//
//        var atual = this.pessoaService.update("1", pessoaRequestDTO);
//        var stubExpect = pessoaResponseDTO;
//
//        assertEquals(stubExpect, atual);
//    }
//
//    @Test
//    void whenDeleteByIdShouldReturnNoContent() {
//
//        when(pessoaRepository.existsById("1")).thenReturn(true);
//        pessoaService.deleteById("1");
//        verify(pessoaRepository, times(1)).deleteById("1");
//
//    }
//
//    @Test
//    void whenDeleteManyShouldReturnNoContent() {
//
//        List<Pessoa> pessoas = listPessoa;
//        List<String> ids = Arrays.asList("1", "2", "3");
//
//        when(pessoaRepository.findAllById(ids)).thenReturn(pessoas);
//
//        var atual = this.pessoaService.deleteMany(ids);
//        var stubExpect = pessoas;
//
//        assertEquals(stubExpect, atual);
//    }
//
//    Endereco endereco = Endereco.builder()
//            .logradouro("Rua Brasil")
//            .bairro("Porto Branco")
//            .cep(12345678)
//            .number(1234)
//            .build();
//
//    Pessoa pessoa = Pessoa.builder()
//            .id("1")
//            .name("Arthur")
//            .email("arthur@gmail.com")
//            .senha("1234")
//            .endereco(endereco)
//            .build();
//
//    PessoaRequestDTO pessoaRequestDTO = PessoaRequestDTO.builder()
//            .name("Arthur")
//            .email("arthur@gmail.com")
//            .senha("1234")
//            .build();
//
//    PessoaResponseDTO pessoaResponseDTO = PessoaResponseDTO.builder()
//            .id("1")
//            .email("arthur@gmail.com")
//            .name("Arthur")
//            .build();
//
//
//    public List<Pessoa> listPessoa = Arrays.asList(
//            Pessoa.builder().name("Arthur").email("arthur@gmail.com").senha("123").build(),
//            Pessoa.builder().name("Carlos").email("carlos@gmail.com").senha("456").build(),
//            Pessoa.builder().name("Magda").email("magda@gmail.com").senha("789").build());
//
//}
