package br.com.forttiori.service.dto;

import br.com.forttiori.entity.Pessoa;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class MapperService {

    private MapperService(){}

    public static ServiceResponseDTO mapToPessoaServiceResponse(Pessoa pessoa) {
        return Optional.ofNullable(pessoa)
                .map(pessoaEntity -> ServiceResponseDTO.builder()
                    .name(pessoaEntity.getName())
                    .email(pessoaEntity.getEmail())
                    .id(pessoaEntity.getId())
                    .build())
                .orElse(null);
    }

    public static List<ServiceResponseDTO> mapListPessoaToListResponse(List<Pessoa> pessoas){
        return pessoas.stream()
                .map(MapperService::mapToPessoaServiceResponse)
                .collect(Collectors.toList());

    }
    public static Pessoa mapPessoaToEntity (ServiceRequestDTO serviceRequestDTO) {
        return Optional.ofNullable(serviceRequestDTO)
                .map(pessoaDTO -> Pessoa.builder()
                    .name(pessoaDTO.getName())
                    .email(pessoaDTO.getEmail())
                    .senha(pessoaDTO.getSenha())
                    .build())
                .orElseThrow(IllegalArgumentException::new);
    }
}
