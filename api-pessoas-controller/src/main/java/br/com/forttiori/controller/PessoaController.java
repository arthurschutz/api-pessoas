package br.com.forttiori.controller;

import br.com.forttiori.controller.dto.ControllerRequestDTO;
import br.com.forttiori.controller.dto.ControllerResponseDTO;
import br.com.forttiori.controller.dto.MapperController;
import br.com.forttiori.entity.Pessoa;
import br.com.forttiori.service.PessoaService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.NO_CONTENT;

@RestController
@RequestMapping("/pessoas")
@RequiredArgsConstructor
public class PessoaController {

    private final PessoaService pessoaService;

    @PostMapping
    @ResponseStatus(CREATED)
    public ControllerResponseDTO save(@RequestBody @Valid ControllerRequestDTO controllerRequestDTO) {
        return MapperController.mapToControllerResponse(pessoaService.save(MapperController.mapPessoaToEntity(controllerRequestDTO)));
    }

    @GetMapping("/todos")
    public List<ControllerResponseDTO> findAll(){
        return this.pessoaService.findAll().stream()
                .map(MapperController::mapToControllerResponse)
                .collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    public ControllerResponseDTO findById(@Valid  @PathVariable String id) {
        return MapperController.mapToControllerResponse(pessoaService.findByIdResponse(id));
    }

    @PutMapping("/{id}")
    public ControllerResponseDTO update(@Valid @PathVariable String id, @RequestBody ControllerRequestDTO controllerRequestDTO) {
        return MapperController.mapToControllerResponse(
                pessoaService.update(id, MapperController.mapPessoaToEntity(controllerRequestDTO)));
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(NO_CONTENT)
    public void delete(@PathVariable String id) {
        pessoaService.deleteById(id);
    }

    @DeleteMapping
    @ResponseStatus(NO_CONTENT)
    public List<Pessoa> deleteMany(@RequestParam("ids") List<String> ids) {
        return pessoaService.deleteMany(ids);
    }
}

