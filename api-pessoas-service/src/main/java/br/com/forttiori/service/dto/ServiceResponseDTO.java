package br.com.forttiori.service.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ServiceResponseDTO {

    private String id;
    private String name;
    private String email;
}
