package br.com.forttiori.controller.dto;

import br.com.forttiori.service.dto.ServiceRequestDTO;
import br.com.forttiori.service.dto.ServiceResponseDTO;

import java.util.Optional;

public class MapperController {

    private MapperController() {
    }

    public static ControllerResponseDTO mapToControllerResponse(ServiceResponseDTO serviceResponseDTO) {
        return Optional.ofNullable(serviceResponseDTO)
                .map(pessoaContrato -> ControllerResponseDTO.builder()
                        .name(pessoaContrato.getName())
                        .email(pessoaContrato.getEmail())
                        .id(pessoaContrato.getId())
                        .build())
                .orElse(null);
    }

    public static ServiceRequestDTO mapPessoaToEntity (ControllerRequestDTO controllerRequestDTO) {
        return Optional.ofNullable(controllerRequestDTO)
                .map(pessoaDTO -> ServiceRequestDTO.builder()
                        .name(pessoaDTO.getName())
                        .email(pessoaDTO.getEmail())
                        .senha(pessoaDTO.getSenha())
                        .build())
                .orElseThrow(IllegalArgumentException::new);
    }
}
