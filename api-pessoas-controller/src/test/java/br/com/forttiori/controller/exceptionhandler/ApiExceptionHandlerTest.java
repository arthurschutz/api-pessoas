package br.com.forttiori.controller.exceptionhandler;

import br.com.forttiori.exceptions.PessoaNaoEncontradaException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {ApiExceptionHandler.class})
class ApiExceptionHandlerTest {

    @Autowired
    ApiExceptionHandler exceptionHandler;

    @Test
    void shouldReturnPessoaNaoEncontrada() {
        var stubExpected = StandardError.builder()
                .status(HttpStatus.NOT_FOUND.value())
                .message("Pessoa Nao Encontrada")
                .build();
        var stubActual = exceptionHandler.pessoaNaoEncontrado(new PessoaNaoEncontradaException("Pessoa Nao Encontrada"));
        assertEquals(stubExpected, stubActual);
    }

    @Test
    void shouldReturnException() {
        var stubExpected = StandardError.builder()
                .status(HttpStatus.INTERNAL_SERVER_ERROR.value())
                .build();
        var stubActual = exceptionHandler.handleGeneralExceptions(new Exception());
        assertEquals(stubExpected,stubActual);
    }

    @Test
    void shouldReturnRuntimeException() {
        var stubExpected = StandardError.builder()
                .status(HttpStatus.INTERNAL_SERVER_ERROR.value())
                .build();
        var stubActual = exceptionHandler.handleRuntimeExceptions(new RuntimeException());
        assertEquals(stubExpected,stubActual);
    }

    @Test
    void shouldReturnIoException() {
        var stubExpected = StandardError.builder()
                .status(HttpStatus.INTERNAL_SERVER_ERROR.value())
                .build();
        var stubActual = exceptionHandler.handleIoExceptionExceptions(new IOException());
        assertEquals(stubExpected,stubActual);
    }
}
