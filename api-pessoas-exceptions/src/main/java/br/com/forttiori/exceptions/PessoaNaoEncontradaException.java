package br.com.forttiori.exceptions;

import java.util.function.Supplier;

public class PessoaNaoEncontradaException extends RuntimeException implements Supplier<PessoaNaoEncontradaException> {

    public PessoaNaoEncontradaException(String message) {
        super(message);
    }

    @Override
    public PessoaNaoEncontradaException get() {
        return this;
    }
}
